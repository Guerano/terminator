#include <stdio.h>

// fibo(0) = 0;
// fibo(1) = 1;
// fibo(n) = fibo(n - 1) + fibo(n - 2);
static unsigned long fibonacci(unsigned long n)
{
    unsigned long n_1 = 1;
    unsigned long n_2 = 0;

    for (int i = 0; i + 2 <= n; i = i + 2)
    {
        n_2 = n_2 + n_1;
        n_1 = n_2 + n_1;
    }
    return n % 2 == 0 ? n_2 : n_1;
}

int main(void)
{
    printf("fibo de %d=%lu\n", 6, fibonacci(100));
    printf("int:%d, lu:%d, llu:%d\n", sizeof (int), sizeof (unsigned long), sizeof (unsigned long long));
}
