#ifndef FONCTION_H
# define FONCTION_H

typedef struct fonction
{
    char *name;
    char *body;
    struct fonction *next;
} s_fonction;

s_fonction *fonction_add(s_fonction *func, char *name, char *body);

s_fonction *fonction_find(s_fonction *func, char *name);

void fonction_destroy(s_fonction **func);

void fonction_print(s_fonction *func);

#endif /* !FONCTION_H */
