#define _XOPEN_SOURCE 700
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>

#include "fonction.h"
#include "print.h"

char *file_to_string(char *filename)
{
    int fd = open(filename, O_RDONLY);
    if (fd == -1)
    {
        dprintf(2, "%s file doesn't exist !\n", filename);
        return NULL;
    }

    struct stat st;
    fstat(fd, &st);

    int size = st.st_size;
    if (size <= 0)
    {
        dprintf(2, "%s file is empty !\n", filename);
        return NULL;
    }

    char *txt = calloc(size + 1, sizeof (char));
    read(fd, txt, size);
    close(fd);
    return txt;
}

void remove_all_pattern(char *str, char *pattern)
{
    if (!str)
        return;
    if (!pattern)
        return;
    for (int i = 0; str[i]; ++i)
    {
        int start = 0;
        if (str[i] == pattern[0])
        {
            start = 1;
            int cpt_pattern = 0;
            int cpt_str = i;
            while (start && pattern[cpt_pattern] != '\0')
            {
                if (pattern[cpt_pattern] != str[cpt_str])
                    start = !start;
                ++cpt_pattern;
                ++cpt_str;
            }
            if (start)
            {
                int len = strlen(pattern);
                for (int cpt = i; str[cpt] != '\0'; ++cpt)
                {
                    if (str[cpt + len] != '\0')
                        str[cpt] = str[cpt + len];
                    else
                        str[cpt] = '\0';
                }
                --i;
            }
        }
    }
}

static int get_pattern_index(char *str, char *pattern)
{
    for (int i = 0; str[i] != '\0'; ++i)
    {
        if (str[i] == pattern[0])
        {
            int start = 1;
            int cpy_i = i;
            for (int j = 0; pattern[j] != '\0'; ++j, ++cpy_i)
            {
                if (pattern[j] != str[cpy_i])
                {
                    start = 0;
                    break;
                }
            }
            if (start)
                return i;
        }
    }
    return -1;
}

/*
** -1 => 'c' not found
** i => index found
*/
static int get_previous_char_index(char *str, char c, int from)
{
    if (!str)
        return -1;
    for (int i = from - 1; i >= 0; --i)
    {
        if (str[i] == c)
            return i;
    }
    return -1;
}

/*
** -1 => 'c' not found
** i => index found
*/
static int get_next_char_index(char *str, char c, int from)
{
    if (!str)
        return -1;
    for (int i = from; str[i]; ++i)
    {
        if (str[i] == c)
            return i;
    }
    return -1;
}

/*
** -1 → no close bracket
** i → the matching closing bracket
*/
static int get_close_bracket(char *str, int start)
{
    int cpt = 1;
    int i = start + 1;
    while (cpt != 0)
    {
        if (str[i] == '\0')
            return -1;
        if (str[i] == '{'
                && str[i - 1] != '\'' && str[i - 1] != '\"'
                && str[i + 1] != '\'' && str[i + 1] != '\"')
            cpt++;
        else if (str[i] == '}'
                && str[i - 1] != '\'' && str[i - 1] != '\"'
                && str[i + 1] != '\'' && str[i + 1] != '\"')
            cpt--;
        ++i;
    }
    return i - 1;
}

/*
** -1 → no beginning brace
** i → the matching beginning brace
*/
static int get_brace_begin(char *str, int start)
{
    int cpt = 1;
    int i = start - 1;
    while (cpt != 0)
    {
        if (i < 0)
            return -1;
        if (str[i] == '(')
            cpt--;
        else if (str[i] == ')')
            cpt++;
        --i;
    }
    return i + 1;
}

static char *get_main(char *str)
{
    int main_index = get_pattern_index(str, "int main(");
    if (main_index == -1)
        return NULL;
    int main_start =
        get_previous_char_index(str, '\n', main_index) + 1;
    int first_bracket = get_next_char_index(str, '{', main_start);
    int main_end = get_close_bracket(str, first_bracket);
    int length = main_end - main_start + 1;
    char *main = calloc(length + 1, sizeof (char));
    main = strncpy(main, &str[main_start], length);
    main[length] = '\0';
    return main;
}

void remove_main(char *str)
{
    char *pattern = get_main(str);
    remove_all_pattern(str, pattern);
}

static int max(int a, int b)
{
    return a > b ? a : b;
}

/*
** -1 → it is not a function
** i → the index of the beginning of the first function found
*/
static int is_a_function(char *str, int index)
{
    if (index <= 0)
        return -1;
    int i = index - 1;
    while ((str[i] == ' ' || str[i] == '\t') && i != 0)
        i--;
    if (i == 0)
        return -1;
    while (str[i] == '\n')
        i--;
    while ((str[i] == ' ' || str[i] == '\t') && i != 0)
        i--;
    if (i == 0 || str[i] != ')')
        return -1;
    int brace_begin = get_brace_begin(str, i);
    int j = brace_begin - 1;
    while ((str[j] == ' ' || str[j] == '\t') && j != 0)
        --j;
    if (j == 0)
        return -1;
    int word_begin = max(get_previous_char_index(str, ' ', j) + 1,
            get_previous_char_index(str, '\n', j) + 1);
    int length = j - word_begin + 1;
    if (strncmp(&str[word_begin], "if", length) == 0)
        return -1;
    else if (strncmp(&str[word_begin], "while", length) == 0)
        return -1;
    else if (strncmp(&str[word_begin], "for", length) == 0)
        return -1;
    else if (strncmp(&str[word_begin], "switch", length) == 0)
        return -1;
    int line_start = get_previous_char_index(str, '\n',
            word_begin) + 1;
    while (str[line_start] == ' ' || str[line_start] == '\t')
        line_start++;
    return line_start;
}

/*
** NULL → no function found
** char * → first function found
*/
static char *get_first_function(char *str, int start_index)
{
    int first_bracket = get_next_char_index(str, '{', start_index);
    if (first_bracket == -1)
        return NULL;
    int func_start = is_a_function(str, first_bracket);
    if (func_start == -1)
        return get_first_function(str, first_bracket + 1);
    int close_bracket = get_close_bracket(str, first_bracket);
    int length = close_bracket - func_start + 1;
    if (length <= 0)
    {
        printf("A bracket couldnt be found for matching!\n");
        return NULL;
    }
    char *func = calloc(length + 1, sizeof (char));
    func = strncpy(func, &str[func_start], length);
    func[length] = '\0';
    return func;
}

char *get_fonction_name(char *func)
{
    int brace_begin = get_next_char_index(func, '(', 0);
    int i = brace_begin - 1;
    while ((func[i] == ' ' || func[i] == '\t') && i != 0)
        --i;
    int name_begin = get_previous_char_index(func, ' ', i) + 1;
    int length = i - name_begin + 1;
    char *name = calloc(length + 1, sizeof (char));
    name = strncpy(name, &func[name_begin], length);
    name[length] = '\0';
    return name;
}

s_fonction *get_all_fonctions(char *str, s_fonction *func)
{
    char *first_func = get_first_function(str, 0);
    while (first_func)
    {
        char *name = get_fonction_name(first_func);
        int body_start = get_next_char_index(first_func, '{', 0);
        func = fonction_add(func, name, &first_func[body_start]);
        remove_all_pattern(str, first_func);
        first_func = get_first_function(str, 0);
    }
    return func;
}

char *clean_filename(char *filename)
{
    int length = strlen(filename);
    int filename_start = get_previous_char_index(
            filename, '/', length - 1) + 1;
    int name_len = length - filename_start;
    char *name = calloc(name_len + 1, sizeof (char));
    name = strncpy(name, &filename[filename_start], name_len);
    name[name_len] = '\0';
    return name;
}

static int get_first_quote_index(char *str, int start)
{
    int first_quote = get_next_char_index(str, '\"', start);
    if (first_quote == -1 || first_quote == 0)
        return first_quote;
    if (str[first_quote - 1] == '\\')
        return get_first_quote_index(str, first_quote + 1);
    return first_quote;
}

static char *get_first_quote(char *str)
{
    int first_quote = get_first_quote_index(str, 0);
    if (first_quote == -1)
        return NULL;
    int end_quote = get_first_quote_index(str, first_quote + 1);
    if (end_quote == -1)
    {
        warning();
        fprintf(stderr, "A quote has not been terminated ! ");
        warning();
        fprintf(stderr, "\n");
        return NULL;
    }
    int len = end_quote - first_quote + 1;
    char *quote = calloc(len + 1, sizeof (char));
    strncpy(quote, &str[first_quote], len);
    quote[len] = '\0';
    return quote;
}

void remove_all_strings(char *str)
{
    char *quote = get_first_quote(str);
    while (quote)
    {
        remove_all_pattern(str, quote);
        quote = get_first_quote(str);
    }
}

/*
** 1 → c is contained by tab
** 0 → it is not
*/
/*static int contains(char *tab, char c)
{
    for (int i = 0; tab[i]; ++i)
        if (tab[i] == c)
            return 1;
    return 0;
}*/
