#include <stdio.h>
#include "tools_string.h"
#include "fonction.h"
#include "process.h"
#include "main.h"

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Need TWO files to compare !\n");
        return 1;
    }
    int mode;
    if (argc < 4)
        mode = AUTO;
    else
        mode = atoi(argv[3]);

    char *file1 = file_to_string(argv[1]);
    //char *file2 = file_to_string(argv[2]);
    //s_fonction *func1 = NULL;
    //s_fonction *func2 = NULL;
    //func1 = get_all_fonctions(file1, func1);
    //func2 = get_all_fonctions(file2, func2);
    //fonction_print(func1);
    //fonction_compare(argv[1], argv[2], func1, func2, mode);
    //printf("FILE BEFORE:\n%s\n", file1);
    remove_all_strings(file1);
    printf("FILE AFTER:\n%s\n", file1);
}
