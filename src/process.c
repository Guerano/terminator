#include <stdlib.h>
#include <stdio.h>

#include "fonction.h"
#include "tools_string.h"
#include "print.h"
#include "main.h"

void fonction_compare(char *filename1, char *filename2,
        s_fonction *func1, s_fonction *func2, int mode)
{
    for (s_fonction *func = func1; func; func = func->next)
    {
        s_fonction *func_match = fonction_find(func2, func->name);
        if (func_match)
            if (strcmp(func->body, func_match->body) == 0)
            {
                if (mode == INTERACTIVE)
                    fgetc(stdin);
                print_match(clean_filename(filename1),
                        clean_filename(filename2),
                        func->name, func_match->name);
            }
    }
}
