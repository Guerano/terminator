#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "fonction.h"


static s_fonction *fonction_create(char *name, char *body)
{
    s_fonction *func = calloc(1, sizeof (s_fonction));
    func->name = strdup(name);
    func->body = strdup(body);
    func->next = NULL;
    return func;
}

s_fonction *fonction_add(s_fonction *func, char *name, char *body)
{
    if (!func)
        return fonction_create(name, body);
    s_fonction *cpy = func;
    while (cpy->next)
        cpy = cpy->next;
    s_fonction *new = calloc(1, sizeof (s_fonction));
    new->name = strdup(name);
    new->body = strdup(body);
    new->next = NULL;
    cpy->next = new;
    return func;
}

s_fonction *fonction_find(s_fonction *func, char *name)
{
    while (func)
    {
        if (strcmp(name, func->name) == 0)
            return func;
        func = func->next;
    }
    return NULL;
}

void fonction_destroy(s_fonction **func)
{
    if (*func)
    {
        fonction_destroy(&(*func)->next);
        void *tmp = &(*func)->name;
        (*func)->name = NULL;
        free(tmp);
        tmp = (*func)->body;
        (*func)->body = NULL;
        free(tmp);
        tmp = *func;
        *func = NULL;
        free(tmp);
    }
}

void fonction_print(s_fonction *func)
{
    if (!func)
        return;
    printf("%s\n", func->name);
    func = func->next;
    while (func)
    {
        printf("\t↓\n");
        printf("%s\n", func->name);
        func = func->next;
    }
}
