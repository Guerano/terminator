#ifndef TOOLS_STRING_H
# define TOOLS_STRING_H

# define _XOPEN_SOURCE 700

# include <string.h>
# include <fcntl.h>
# include <unistd.h>
# include <sys/stat.h>
# include <stdio.h>
# include <stdlib.h>

# include "fonction.h"

char *file_to_string(char *filename);

char *remove_all_pattern(char *str, char *pattern);

void remove_main(char *str);

s_fonction *get_all_fonctions(char *str, s_fonction *func);

char *clean_filename(char *filename);

void remove_all_strings(char *str);

#endif /* !TOOLS_STRING_H */
