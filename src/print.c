#include <stdio.h>

#define RES  "\x1B[0m"
#define BLA  "\x1B[30m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define YEL  "\x1B[33m"
#define BLU  "\x1B[34m"
#define MAG  "\x1B[35m"
#define CYN  "\x1B[36m"
#define WHT  "\x1B[37m"

#define GREY "\x1B[30;1m"
#define REDB "\x1B[31;1m"
#define GRNB "\x1B[32;1m"
#define YELB "\x1B[33;1m"
#define BLUB "\x1B[34;1m"
#define MAGB "\x1B[35;1m"
#define CYNB "\x1B[36;1m"
#define WHTB "\x1B[37;1m"

void print_match(char *filename1, char *filename2,
        char *func1, char *func2)
{
    printf(BLUB "%-20.20s " RES, func1);
    printf("(%-15.15s)  | ", filename1);
    printf(BLUB "%-20.20s " RES, func2);
    printf("(%-15.15s)   ", filename2);
    printf(REDB "[MATCH]\n" RES);
}

void warning(void)
{
    fprintf(stderr, REDB "/!\\ " RES);
}
