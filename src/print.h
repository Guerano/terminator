#ifndef PRINT_H
# define PRINT_H

void print_match(char *filename1, char *filename2,
        char *func1, char *func2);

void warning(void);

#endif /* !PRINT_H */
