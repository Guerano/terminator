CC?=gcc
CFLAGS=-Wall -Wextra -Werror -pedantic -std=c99 -g3
LDFLAGS=-shared
SRC_DIR=src/
CHK_DIR=check/
LIB=
SRC=main.c tools_string.c fonction.c process.c print.c
CHK_SRC=check.c
CHK_OBJ=$(CHK_SRC:.c=.o)
OBJ=$(SRC:.c=.o)
BIN=termine
CHK=mouline

all:$(addprefix $(DIR),$(OBJ)) $(BIN)

%.o:$(SRC_DIR)%.c
	$(CC) $(CFLAGS) -c $^ -o $(SRC_DIR)$@

exec:$(addprefix $(SRC_DIR),$(OBJ)) $(BIN)
	./$(BIN)

$(BIN):$(addprefix $(SRC_DIR),$(OBJ))
	$(CC) $(CFLAGS) -o $@ $^

check:$(addprefix $(CHK_DIR),$(CHK_OBJ)) build_check

$(CHK_DIR)%.o:$(CHK_DIR)%.c
	$(CC) $(CFLAGS) -c $^ -o $@

build_check:$(addprefix $(CHK_DIR),$(CHK_OBJ))
	$(CC) $(CFLAGS) -o $(CHK) $^

clean:
	rm -rf $(BIN) $(addprefix $(SRC_DIR),$(OBJ)) $(LIB)
	rm -rf $(addprefix $(CHK_DIR),$(CHK_OBJ)) $(CHK)
	rm -rf *.sw?
